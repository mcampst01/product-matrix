#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testExercise_A() {
    // Given
    int* matrixEntrada2[1];
    matrixEntrada2[0] = (int*) malloc(sizeof(int));
    matrixEntrada2[0][0] = -2;

    int* matrixEntrada1[1];
    matrixEntrada1[0] = (int*) malloc(sizeof(int));
    matrixEntrada1[0][0] = -19;

    int* matrixEsperada[1];
    matrixEsperada[0] = (int*) malloc(sizeof(int));
    matrixEsperada[0][0] = 38;
    int productRows;
    int productColumns;

    // When
    int** matrixResultado = product(matrixEntrada1, 1, 1, matrixEntrada2, 1, 1, &productRows, &productColumns);
    
    // Then
    assertMatrixEquals_int(matrixEsperada, 1, 1, matrixResultado, 1, 1);
}

void testExercise_B() {
    // Given
    int* matrixEntrada2[2];
    matrixEntrada2[0] = (int*) malloc(sizeof(int));
    matrixEntrada2[0][0] = -20;
    matrixEntrada2[1] = (int*) malloc(sizeof(int));
    matrixEntrada2[1][0] = 12;

    int* matrixEntrada1[2];
    matrixEntrada1[0] = (int*) malloc(2*sizeof(int));
    matrixEntrada1[0][0] = 27;
    matrixEntrada1[0][1] = -15;

    int* matrixEsperada[2];
    matrixEsperada[0] = (int*) malloc(sizeof(int));
    matrixEsperada[0][0] = -720;
    int productRows;
    int productColumns;

    // When
    int** matrixResultado = product(matrixEntrada1, 1, 2, matrixEntrada2, 2, 1, &productRows, &productColumns);
    
    // Then
    assertMatrixEquals_int(matrixEsperada, 1, 2, matrixResultado, 1, 2);
}

void testExercise_C() {
    // Given
    int* matrixEntrada1[2];
    matrixEntrada1[0] = (int*) malloc(sizeof(int));
    matrixEntrada1[0][0] = 1;
    matrixEntrada1[1] = (int*) malloc(sizeof(int));
    matrixEntrada1[1][0] = -12;

    int* matrixEntrada2[1];
    matrixEntrada2[0] = (int*) malloc(2*sizeof(int));
    matrixEntrada2[0][0] = -10;
    matrixEntrada2[0][1] = -4;

    int* matrixEsperada[2];
    matrixEsperada[0] = (int*) malloc(2*sizeof(int));
    matrixEsperada[0][0] = -10;
    matrixEsperada[0][1] = 4;
    matrixEsperada[1] = (int*) malloc(2*sizeof(int));
    matrixEsperada[1][0] = 120;
    matrixEsperada[1][1] = 48;

    int productRows;
    int productColumns;

    // When
    int** matrixResultado = product(matrixEntrada1, 1, 2, matrixEntrada2, 2, 1, &productRows, &productColumns);
    
    // Then
    assertMatrixEquals_int(matrixEsperada, 2, 2, matrixResultado, 2, 2);
}

void testExercise_D() {
    // Given
    int* matrixEntrada1[2];
    matrixEntrada1[0] = (int*) malloc(2*sizeof(int));
    matrixEntrada1[0][0] = 13;
    matrixEntrada1[0][1] = -11;
    matrixEntrada1[1] = (int*) malloc(2*sizeof(int));
    matrixEntrada1[1][0] = -8;
    matrixEntrada1[1][1] = -6;
    
    int* matrixEntrada2[2];
    matrixEntrada2[0] = (int*) malloc(2*sizeof(int));
    matrixEntrada2[0][0] = 28;
    matrixEntrada2[0][1] = -18;
    matrixEntrada2[1] = (int*) malloc(2*sizeof(int));
    matrixEntrada2[1][0] = 29;
    matrixEntrada2[1][1] = -9;

    int* matrixEsperada[2];
    matrixEsperada[0] = (int*) malloc(2*sizeof(int));
    matrixEsperada[0][0] = 45;
    matrixEsperada[0][1] = -135;
    matrixEsperada[1] = (int*) malloc(2*sizeof(int));
    matrixEsperada[1][0] = 398;
    matrixEsperada[1][1] = 198;

    int productRows;
    int productColumns;

    // When
    int** matrixResultado = product(matrixEntrada1, 2, 2, matrixEntrada2, 2, 2, &productRows, &productColumns);
    
    // Then
    assertMatrixEquals_int(matrixEsperada, 2, 2, matrixResultado, 2, 2);
}

void testExercise_E() {
    // Given
    int* matrixEntrada1[3];
    matrixEntrada1[0] = (int*) malloc(2*sizeof(int));
    matrixEntrada1[0][0] = 14;
    matrixEntrada1[0][1] = -7;
    matrixEntrada1[1] = (int*) malloc(2*sizeof(int));
    matrixEntrada1[1][0] = -5;
    matrixEntrada1[1][1] = -17;
    matrixEntrada1[2] = (int*) malloc(2*sizeof(int));
    matrixEntrada1[2][0] = -16;
    matrixEntrada1[2][1] = -14;
    
    int* matrixEntrada2[2];
    matrixEntrada2[0] = (int*) malloc(3*sizeof(int));
    matrixEntrada2[0][0] = -3;
    matrixEntrada2[0][1] = 15;
    matrixEntrada2[0][2] = -1;
    matrixEntrada2[1] = (int*) malloc(3*sizeof(int));
    matrixEntrada2[1][0] = 0;
    matrixEntrada2[1][1] = -13;
    matrixEntrada2[0][2] = 2;


    int* matrixEsperada[3];
    matrixEsperada[0] = (int*) malloc(3*sizeof(int));
    matrixEsperada[0][0] = -42;
    matrixEsperada[0][1] = 301;
    matrixEsperada[0][2] = 28;
    matrixEsperada[1] = (int*) malloc(3*sizeof(int));
    matrixEsperada[1][0] = 15;
    matrixEsperada[1][1] = 146;
    matrixEsperada[1][2] = -29;
    matrixEsperada[2] = (int*) malloc(3*sizeof(int));
    matrixEsperada[2][0] = 48;
    matrixEsperada[2][1] = -58;
    matrixEsperada[2][2] = -12;

    int productRows;
    int productColumns;

    // When
    int** matrixResultado = product(matrixEntrada1, 2, 3, matrixEntrada2, 3, 2, &productRows, &productColumns);
    
    // Then
    assertMatrixEquals_int(matrixEsperada, 3, 3, matrixResultado, 3, 3);
}
